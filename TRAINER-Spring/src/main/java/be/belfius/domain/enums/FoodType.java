package be.belfius.domain.enums;

public enum FoodType {
    MEAT, VEGGIE, UNKNOWN, FISH, KIBBLE;
}
