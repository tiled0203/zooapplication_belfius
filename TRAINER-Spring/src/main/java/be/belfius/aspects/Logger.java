package be.belfius.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.io.PrintStream;

@Aspect
@Component // creates a bean out of this class
public class Logger { // this would be our minstrel
    private PrintStream stream;

    public Logger(PrintStream stream) { // we created a PrintStream bean in ZooConfig
        this.stream = stream;
    }

    @Pointcut("execution(* be.belfius.repositories.jdbc.AnimalRepositoryImpl.findAll(..))")
    private void logThis(){}

    @Before("logThis()")
    public void logBeforeThisMethod(){
        stream.println("getLogger is been called");
    }

    @After("logThis()")
    public void logAfterThisMethod(){
        stream.println("getAnimals is finished executing");
    }
}
