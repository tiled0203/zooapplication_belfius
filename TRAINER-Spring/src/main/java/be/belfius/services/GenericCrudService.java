package be.belfius.services;

import java.util.List;

public interface GenericCrudService<T> {  // renamed myService to GenericCrudService
    List<T> findAll();
    void save(T entity);
    void deleteById(int id);
    T findByName(String name);
    int count();
}
