package be.belfius.services;

import be.belfius.domain.Animal;
import be.belfius.domain.Food;
import be.belfius.domain.enums.AnimalType;
import be.belfius.domain.enums.FoodType;
import javassist.NotFoundException;

import java.util.List;

public interface FoodDistributionService {
    List<Food> findFoodByAnimalType(AnimalType animalType);
    void feedAnimals(List<Animal> animals);

    List<Food> findAllFoods();

    Food findFoodById(int id) throws NotFoundException;

    List<FoodType> findAllAvailableFoodTypesByAnimalType(AnimalType animalType);

    List<FoodType> findAllAvailableFoodTypes();
}

