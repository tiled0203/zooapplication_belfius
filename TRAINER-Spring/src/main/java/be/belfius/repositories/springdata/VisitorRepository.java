package be.belfius.repositories.springdata;

import be.belfius.domain.Visitor;
import be.belfius.domain.VisitorNameAddressId;
import be.belfius.domain.enums.VisitorType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface VisitorRepository extends JpaRepository<Visitor, VisitorNameAddressId> {
    List<Visitor> findVisitorsByVisitorType(VisitorType visitorType);

    @Query("select distinct v.visitorType from Visitor v ")
    List<VisitorType> findDistinctVisitorTypes();


    Optional<Visitor> findByVisitorNameAddressId(VisitorNameAddressId visitorNameAddressId);
}
