package be.belfius.repositories.jpa;

import be.belfius.domain.Animal;
import be.belfius.repositories.GenericCrudRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Qualifier("animal")
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jpa")
@Transactional
public class AnimalRepositoryImpl implements GenericCrudRepository<Animal> {

    private Logger logger = LoggerFactory.getLogger(AnimalRepositoryImpl.class.getName());

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Animal entity) {
        if (entity.getId() != 0) { // if the entity has an id so != 0 then tis is an update (spring data handles this automatically )
            entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }
    }

    @Override
    public List<Animal> findAll() {
        return entityManager.createNamedQuery("findAllAnimals", Animal.class).getResultList();
    }

    @Override
    public void delete(Animal entity) {

    }

    @Override
    public Animal findByName(String name) {
        try {
            return entityManager
                    .createQuery("select a from Animal a where a.name like :name", Animal.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            logger.error("Cannot find animal {}", name);
            return null;
        }
    }
}
