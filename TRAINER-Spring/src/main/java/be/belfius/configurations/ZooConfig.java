package be.belfius.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.PrintStream;

@Configuration
@EnableAspectJAutoProxy
@EnableSwagger2
public class ZooConfig {

    // not needed anymore because we are using the database
//    @Bean
//    @Profile({"prod", "dev"})
//    @Qualifier("animal")
//    public MyRepository<Animal> myRepository() {
//        MyRepository<Animal> myRepository = new AnimalRepositoryImpl();
//        Bear bear = new Bear("Baloe"); // preconfigured test data in repository
//        Lion lion = new Lion("Simba");
//        Giraffe giraffe = new Giraffe("Long neck");
//        bear.setDateOfBirth(LocalDate.of(2000, 2, 2));
//        myRepository.saveEntity(bear);
//        myRepository.saveEntity(lion);
//        myRepository.saveEntity(giraffe);
//        return myRepository;
//    }

//    @Bean
//    public FoodRepository addingFoodToRepo() {
//        //        foodRepository.addFoodForAnimalType(Bear.class, new Food(1, FoodType.FISH, "Salmon"));    // not needed anymore because we are using the database
////        foodRepository.addFoodForAnimalType(Lion.class, new Food(2, FoodType.MEAT, "Beef"));    // not needed anymore because we are using the database
////        foodRepository.addFoodForAnimalType(Giraffe.class, new Food(3, FoodType.VEGGIE, "Leaf"));    // not needed anymore because we are using the database
//        return new FoodRepositoryImpl();
//    }
//
//    @Bean
//    @IsKibble
//    public FoodRepository kibbleFoodRepository() {
//        //        foodRepository.addFoodForAnimalType(Bear.class, new Kibble());    // not needed anymore because we are using the database
////        foodRepository.addFoodForAnimalType(Lion.class, new Kibble());    // not needed anymore because we are using the database
////        foodRepository.addFoodForAnimalType(Giraffe.class, new Kibble());    // not needed anymore because we are using the database
////        foodRepository.addFoodForAnimalType(Dog.class, new Kibble());    // not needed anymore because we are using the database
//        return new KibbleFoodRepositoryImpl();
//    }

    @Bean
    public PrintStream stream() {
        return System.out;
    }


//    @Bean // 2nd way to configure datasource (easiest way is application.properties)
//    public DataSource dataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//        dataSource.setPassword("P@ssw0rd");
//        dataSource.setUsername("root");
//        dataSource.setUrl("jdbc:mysql://localhost:3306/zoo");
//        return dataSource;
//    }


}
