package be.belfius.zoo.repository;

import be.belfius.zoo.domain.Animal;
import be.belfius.zoo.domain.Bear;
import be.belfius.zoo.services.AnimalService;
import be.belfius.zoo.util.TestDatabase;
import org.dbunit.DatabaseUnitException;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.*;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;

public class AnimalRepositoryDbTest {
    private IDataSet dataSetA;
    private AnimalService animalService = new AnimalService();
    private static JdbcDatabaseTester jdbcDatabaseTester;
    private static IDatabaseConnection iDatabaseConnection;
    private IDataSet xlsDataSet;

    @BeforeClass
    public static void createConnection() throws SQLException, DatabaseUnitException, ClassNotFoundException {
        /*jdbcDatabaseTester = new TestDatabase().createIDatabaseTesterConnection(); //using iDataBaseTester
        jdbcDatabaseTester.setSetUpOperation(DatabaseOperation.UPDATE); //using iDataBaseTester
        jdbcDatabaseTester.setTearDownOperation(DatabaseOperation.REFRESH); //using iDataBaseTester */
        //using normal IDataseConnection
        iDatabaseConnection = new DatabaseConnection(new TestDatabase().createConnection());
    }

    @Before
    public void setup() throws Exception {
        URL url = getClass().getClassLoader().getResource("data/Animal.xls");
        xlsDataSet = new XlsDataSet(new File(url.getPath()));
        DatabaseOperation.UPDATE.execute(iDatabaseConnection, xlsDataSet);
        dataSetA = iDatabaseConnection.createDataSet(new String[]{"Animal"});

        /* using iDataBaseTester
        jdbcDatabaseTester.setDataSet(xlsDataSet);
        dataSetA = jdbcDatabaseTester.getDataSet();
        jdbcDatabaseTester.onSetup();*/
    }

    @After
    public void after() throws Exception {
        //jdbcDatabaseTester.onTearDown();//using iDataBaseTester
        DatabaseOperation.CLEAN_INSERT.execute(iDatabaseConnection, xlsDataSet); // this will reset the database to it begin state
    }

    @Test
    public void assertDeleteAnimals() throws DatabaseUnitException, MalformedURLException, SQLException {
//        ITable animalTable = jdbcDatabaseTester.getDataSet().getTable("Animal");//using iDataBaseTester
        ITable animalTable = dataSetA.getTable("Animal"); // fetch it the first time from the database
        Assert.assertEquals(1, animalTable.getValue(0, "id"));
        Bear bear = new Bear();
        bear.setId(1);
        animalService.deleteAnimal(bear);
        animalTable = dataSetA.getTable("Animal");  // fetch it the second time from the database, because we deleted an animal
        Assert.assertNotEquals(1, animalTable.getValue(0, "id"));
    }

    @Test
    public void assertAnimalsFromDbAndService() throws DataSetException {
        List<Animal> animals = animalService.getAnimals();
        ITable animalTable = dataSetA.getTable("Animal"); // fetch it the first time from the database
        Assert.assertEquals(animals.size(), animalTable.getRowCount());
    }


}
