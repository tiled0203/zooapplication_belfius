package be.belfius.zoo.services;

import be.belfius.zoo.domain.Visitor;
import be.belfius.zoo.domain.enums.VisitorType;
import be.belfius.zoo.util.Database;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.*;

import java.io.File;
import java.net.URL;

public class TicketServiceTest {
    private static DatabaseConnection iDatabaseConnection;
    private TicketService ticketService = new TicketService();
    private XlsDataSet xlsDataSet;
    private IDataSet dataSetA;

    @BeforeClass
    public static void beforeClass() throws Exception {
        iDatabaseConnection = new DatabaseConnection(new Database().createConnection());
    }

    @Before
    public void setUp() throws Exception {
        URL url = getClass().getClassLoader().getResource("data/Ticket.xls");
        xlsDataSet = new XlsDataSet(new File(url.getPath()));
        DatabaseOperation.CLEAN_INSERT.execute(iDatabaseConnection, xlsDataSet);
        dataSetA = iDatabaseConnection.createDataSet(new String[]{"Ticket"});
    }

    @After
    public void tearDown() throws Exception {
        DatabaseOperation.CLEAN_INSERT.execute(iDatabaseConnection, xlsDataSet);
    }

    @Test
    public void checkThatCalculatedReductionPriceForAdultIsCorrect() {
        Visitor visitor = new Visitor("test", VisitorType.ADULT);
        double calculatePriceForVistitor = ticketService.calculateReductionPriceForVisitorType(visitor);
        Assert.assertEquals(21, calculatePriceForVistitor, 0);
    }

    @Test
    public void checkThatCalculatedReductionPriceForChildIsCorrect() {
        Visitor visitor = new Visitor("test", VisitorType.CHILD);
        double calculatePriceForVistitor = ticketService.calculateReductionPriceForVisitorType(visitor);
        Assert.assertEquals(9, calculatePriceForVistitor, 0);
    }

    @Test
    public void checkThatCalculatedReductionPriceForHandicapIsCorrect() {
        Visitor visitor = new Visitor("test", VisitorType.HANDICAP);
        double calculatePriceForVistitor = ticketService.calculateReductionPriceForVisitorType(visitor);
        Assert.assertEquals(9, calculatePriceForVistitor, 0);
    }
}