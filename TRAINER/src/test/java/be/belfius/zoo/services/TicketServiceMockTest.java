package be.belfius.zoo.services;

import be.belfius.zoo.domain.Ticket;
import be.belfius.zoo.domain.Visitor;
import be.belfius.zoo.domain.enums.VisitorType;
import be.belfius.zoo.repository.generic_jpa_repositories.TicketRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceMockTest {

    //    @Spy
    @Mock
    // uses the genric_jpa_repositories change the import if you want to use jpa or jdbc
    private TicketRepository ticketRepository;

    @InjectMocks
    private TicketService ticketService = new TicketService();

    @Before
    public void setup() {
        Mockito.when(ticketRepository.findById(1)).thenReturn(new Ticket(80));
        Mockito.when(ticketRepository.findById(2)).thenReturn(new Ticket(20));
//        when(ticketRepository.getPriceByVisitor(Visitor.ADULT)).thenCallRealMethod();
    }

    @Test
    public void verifyGetPriceByAdultVisitorBeenCalledOnce() {
        Visitor visitor = new Visitor("testName", VisitorType.ADULT);
        visitor.setId(1);
        double price = ticketService.calculateReductionPriceForVisitorType(visitor);
        Mockito.verify(ticketRepository, Mockito.times(1)).findById(1);
        Assert.assertEquals(56, price, 0);
        Mockito.verifyNoMoreInteractions(ticketRepository);
    }

    @Test
    public void verifyGetPriceByChildVisitorBeenCalledOnce() {
        Visitor visitor = new Visitor("testName", VisitorType.CHILD);
        double price = ticketService.calculateReductionPriceForVisitorType(visitor);
        Mockito.verify(ticketRepository, Mockito.times(1)).findById(1);
        Assert.assertEquals(12, price, 0);
    }

    @Test
    public void testPrivate() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, ClassNotFoundException {
//        Method testMethod = TicketService.class.getDeclaredMethod("test");
//        testMethod.setAccessible(true);
//        String result = (String) testMethod.invoke(ticketService, null);

        String result = ticketService.test(); // when test method is package private
        Assert.assertEquals("iets", result); // using reflection api

    }

}
