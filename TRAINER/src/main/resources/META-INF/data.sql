insert into Bear(name) values("Amir");
insert into Bear(name) values("Jim");
insert into Bear(name) values("Julien2");

insert into Food(foodName, foodType) values("Salmon", "FISH");
insert into Food(foodName, foodType) values("Beef", "MEAT");
insert into Food(foodName, foodType) values("Leaf", "VEGGIE");

insert into Visitor(name,visitorType)values("Tom", "ADULT");
insert into Visitor(name,visitorType)values("Jan", "ADULT");
insert into Visitor(name,visitorType)values("Liam", "CHILD");

insert into Address(city, houseNumber, street)values ("Kalmthout", "4B" , "testStreet");