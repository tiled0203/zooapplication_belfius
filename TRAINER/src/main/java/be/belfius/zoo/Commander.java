package be.belfius.zoo;

import be.belfius.zoo.command_pattern.commands.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Commander {
    private static Logger logger = LoggerFactory.getLogger(ZooApplication.class.getName());

    public enum Commands {
        SHOW_ANIMAL("Show animals", new ShowAnimalCommand()),
        ADD_ANIMAL("Add new animal", new AddAnimalCommand()),
        REMOVE_ANIMAL("Remove animal", new RemoveAnimalCommand()),
        ADD_TICKET_PRICE("Add ticket price", new AddTicketCommand()),
        FEED_ANIMAL("Feed animals", new FeedAnimalCommand()),
        SHOW_VISITOR("Show visitors", new ShowVisitorsCommand()),
        SHOW_ADDRESSES("Show addressess", new ShowAddressCommand());

        private String commandDescription;
        private Command command; // for the normal command pattern

        Commands(String commandDescription, Command command) {
            this.commandDescription = commandDescription;
            this.command = command;
        }

        public String getCommandDescription() {
            return commandDescription;
        }

        public Command getCommand() {
            return command;
        }
    }
}
