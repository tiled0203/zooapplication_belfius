package be.belfius.zoo.services;

import be.belfius.zoo.domain.Animal;
import be.belfius.zoo.domain.Food;
import be.belfius.zoo.repository.generic_jpa_repositories.AnimalRepository;

import java.util.List;

public class AnimalService {
    // uses the genric_jpa_repositories change the import if you want to use jpa or jdbc
    private AnimalRepository animalRepository = new AnimalRepository();

    public List<Animal> getAnimals() {
        return animalRepository.findAll();
    }


    public void saveAnimal(Animal animal) {
        animalRepository.save(animal);
    }


    public void deleteAnimal(Animal animal) {
        animalRepository.delete(animal);
    }

    public void feedAnimal(Animal animal, Food food) {
        animal.setFood(food);
        animalRepository.save(animal);
    }
}
