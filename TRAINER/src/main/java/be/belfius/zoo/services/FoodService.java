package be.belfius.zoo.services;

import be.belfius.zoo.domain.Animal;
import be.belfius.zoo.domain.Food;
import be.belfius.zoo.repository.generic_jpa_repositories.FoodRepository;

import java.util.List;

public class FoodService {
    // uses the genric_jpa_repositories change the import if you want to use jpa or jdbc
    private final FoodRepository foodRepository = new FoodRepository();

    public List<Food> getFoodNamesByFoodType(Animal animal) {
//        try {
//            Class cls = Class.forName(animal.getClass().getName());
//            Method method = cls.getMethod("getFoodType");
//            FoodType foodType = (FoodType) method.invoke(animal);
//            return foodRepository.getFoodByType(foodType);
//        } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        return null;
        return foodRepository.getFoodByType(animal.getFoodType());
    }

    public List<Food> getAllFood() {
        return foodRepository.findAll();
    }
}
