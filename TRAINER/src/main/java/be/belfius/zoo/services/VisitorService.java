package be.belfius.zoo.services;

import be.belfius.zoo.domain.Visitor;
import be.belfius.zoo.repository.generic_jpa_repositories.VisitorRepository;

import java.util.List;

public class VisitorService {
    // uses the genric_jpa_repositories change the import if you want to use jpa or jdbc
    private VisitorRepository visitorRepository = new VisitorRepository();

    public void saveVisitor(Visitor visitor) {
        visitorRepository.save(visitor);
    }

    public List<Visitor> findVisitorsByName(String name) {
        return visitorRepository.findByName(name);
    }
}
