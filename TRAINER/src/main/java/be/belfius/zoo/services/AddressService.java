package be.belfius.zoo.services;

import be.belfius.zoo.domain.Address;
import be.belfius.zoo.repository.generic_jpa_repositories.AddressRepository;

import java.util.List;

public class AddressService {
    // TODO: uses the genric_jpa_repositories create a normal jpa repository and change the import

    private AddressRepository addressRepository = new AddressRepository();

    public List<Address> findAll(){
        return addressRepository.findAll();
    }

}
