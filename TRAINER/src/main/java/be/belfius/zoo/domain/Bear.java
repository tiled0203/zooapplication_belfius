package be.belfius.zoo.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("BEAR")
//id and all common properties will be inherited from the superclass Animal
public class Bear extends Animal {
    private LocalDate dateOfBirth;

    public Bear() {
    }

    // if you want to use the builder pattern in Animal class
//    Bear(Builder builder) { // because i'm using the builder pattern
//        super(builder);
//    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


}
