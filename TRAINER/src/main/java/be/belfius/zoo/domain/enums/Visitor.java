package be.belfius.zoo.domain.enums;

public enum Visitor {
    ADULT,CHILD,HANDICAP
}
