package be.belfius.zoo.domain;

import be.belfius.zoo.domain.enums.AnimalType;
import be.belfius.zoo.domain.enums.FoodType;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "animalType")
@Filter(name = "limitFilter" , condition = ":limit > (select count(a.id) from Animal a)")
public abstract class Animal extends BaseEntity {
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Food food;

    @Enumerated(EnumType.STRING)
    private FoodType foodType;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private AnimalType animalType;

    @NotEmpty
    @Size(min = 3, max = 200)
    private String name;

    public AnimalType getAnimalType() {
        return animalType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Food getFood() {
        return food;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public void setFood(Food food) {
        this.food = food;
    }


}


// ------ IF YOU WANT TO USE THE BUILDER PATTERN ------
//
//    protected Animal(Builder builder) {
//        this.name = builder.name;
////        this.food = builder.food;
//        this.id = builder.id;
//    }
//
//    protected Animal() {
//    }
//
//    public String getName() {
//        return name;
//    }
//
////    public Food getFood() {
////        return food;
////    }
//
//
//    public int getId() {
//        return id;
//    }
//
//    public AnimalType getAnimalType() {
//        return animalType;
//    }
//
//public static class Builder {
//    private String name;
//    private Food food;
//    private AnimalType animalType;
//    private int id;
//    private Logger logger = LoggerFactory.getLogger(Animal.class.getName());
//
//    public Builder(AnimalType animalType) {
//        this.animalType = animalType;
//    }
//
//    public Builder withId(int id) {
//        this.id = id;
//        return this;
//    }
//
//    public Builder withFood(Food food) {
//        this.food = food;
//        return this;
//    }
//
//    public Builder withName(String name) {
//        this.name = name;
//        return this;
//    }
//
//    public Animal build() {
//        try {
//            switch (this.animalType) {
//                case BEAR:
//                    return new Bear(this);
//                case LION:
//                    return new Lion(this);
//                case DOG:
//                    return new Dog(this);
//                case GIRAFFE:
//                    return new Giraffe(this);
//                default:
//                    throw new ClassNotFoundException(this.animalType + " class does not exist");
//            }
//        } catch (ClassNotFoundException e) {
//            logger.error(this.animalType + " class does not exist", e);
//        }
//        return null;
//    }
//
//
//}
//
//    @Override
//    public String toString() {
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("name:" + name);
////        stringBuilder.append(" food:" + getFood());
//        return stringBuilder.toString();
//    }