package be.belfius.zoo.domain.enums;

public enum VisitorType {
    ADULT,CHILD,HANDICAP
}
