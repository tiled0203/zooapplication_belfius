package be.belfius.zoo.domain.enums;

public enum FoodType {
    MEAT, VEGGIE, UNKNOWN, FISH
}
