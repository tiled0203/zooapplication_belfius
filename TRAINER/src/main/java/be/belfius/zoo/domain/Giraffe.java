package be.belfius.zoo.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("GIRAFFE")
public class Giraffe extends Animal {

    // if you want to use the builder pattern in Animal class
//    protected Giraffe(Builder builder) {
//        super(builder);
//    }

    public Giraffe() {
    }

}
