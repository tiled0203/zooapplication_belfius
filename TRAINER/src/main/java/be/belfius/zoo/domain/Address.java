package be.belfius.zoo.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Builder

public class Address extends BaseEntity {
    private String street;
    private String houseNumber;
    private String city;

}
