package be.belfius.zoo.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("DOG")
public class Dog extends Animal {

    // if you want to use the builder pattern in Animal class
//    protected Dog(Builder builder) {
//        super(builder);
//    }

    public Dog() {
    }

}
