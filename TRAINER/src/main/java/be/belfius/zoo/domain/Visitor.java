package be.belfius.zoo.domain;

import be.belfius.zoo.domain.enums.VisitorType;

import javax.persistence.*;
import java.util.List;

@Entity
@NamedQuery(name = "findVisitorByName", query = "select v from Visitor v where v.name like :name")
public class Visitor extends BaseEntity{

    @Enumerated(EnumType.STRING)
    private VisitorType visitorType;
    private String name;
    //if you see in the ticket class then you see that there is also a property 'visitors'
    //this means that this relationship is Bidirectional and not Unidirectional
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ticket_fk")
    private Ticket ticket; // 0 or many vistors can have one ticket. (Visitors could be also be group or family)

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "visitor_fk")
    private List<Address> address;

    public Visitor() {
    }

    public Visitor(String name, VisitorType visitorType){
        this.name = name;
        this.visitorType = visitorType;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public VisitorType getVisitorType() {
        return visitorType;
    }

    public void setVisitorType(VisitorType visitorType) {
        this.visitorType = visitorType;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }
}
