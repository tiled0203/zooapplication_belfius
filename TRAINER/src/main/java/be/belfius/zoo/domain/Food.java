package be.belfius.zoo.domain;

import be.belfius.zoo.domain.enums.FoodType;

import javax.persistence.*;

@Entity
public class Food extends BaseEntity{
    @Enumerated(EnumType.STRING)
    private FoodType foodType;
    private String foodName;

    public Food(int id, FoodType foodType, String foodName) {
        this.foodType = foodType;
        this.foodName = foodName;
    }

    public Food() {
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    @Override
    public String toString() {
        return getFoodName();
    }
}
