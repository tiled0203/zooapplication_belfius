@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(name = "findAllAnimals", query = "select a from Animal a"),
        @org.hibernate.annotations.NamedQuery(name = "findAnimalByName", query = "select a from Animal a where a.name = :name")
})
@org.hibernate.annotations.FilterDef(
        name = "limitFilter",
        parameters = {
                @org.hibernate.annotations.ParamDef(
                        name = "limit", type = "int" // accept an int
                )
        }
)
package be.belfius.zoo.domain;