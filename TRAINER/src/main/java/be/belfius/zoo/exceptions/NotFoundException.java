package be.belfius.zoo.exceptions;

public class NotFoundException extends Throwable {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException() {
        super();
    }
}
