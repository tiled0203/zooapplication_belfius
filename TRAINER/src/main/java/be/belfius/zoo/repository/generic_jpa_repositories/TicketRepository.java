package be.belfius.zoo.repository.generic_jpa_repositories;

import be.belfius.zoo.domain.Ticket;

//i've converted this class to a jpa repository
public class TicketRepository extends GenericRepository<Ticket, Integer> {

    public TicketRepository() {
        super(Ticket.class);
    }
}
