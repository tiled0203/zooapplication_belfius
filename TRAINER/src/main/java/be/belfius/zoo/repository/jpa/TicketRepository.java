package be.belfius.zoo.repository.jpa;

import be.belfius.zoo.domain.Ticket;
import be.belfius.zoo.util.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.validation.Validation;

//i've converted this class to a jpa repository
public class TicketRepository {
    private Logger logger = LoggerFactory.getLogger(TicketRepository.class.getName());

    public void save(Ticket ticket) { // CREATE
        EntityManager entityManager = Database.getEntityManager();
        Validation.buildDefaultValidatorFactory().getValidator().validate(ticket); // validate bear
        EntityTransaction transaction = entityManager.getTransaction();
        //Use an if statement to check if there are validation errors, if not then save the Bear
        try {
            //begin transaction in case something would break
            transaction.begin();
            //merge will also create or update a query
            //and it will reattach an entity when it is detached
            entityManager.merge(ticket);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            logger.error(e.getMessage());
        }
    }

    public Ticket findById(int ticketId) {
        EntityManager entityManager = Database.getEntityManager();
        return entityManager.createQuery("select t from Ticket t where t.id = :ticketId", Ticket.class)
                .setParameter("ticketId", ticketId)
                .getSingleResult();
    }
}
