package be.belfius.zoo.repository.jpa;

import be.belfius.zoo.domain.Animal;
import be.belfius.zoo.util.Database;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.validation.Validation;
import java.util.List;

public class AnimalRepository {

    public AnimalRepository() {
        EntityManager entityManager = Database.getEntityManager();
        org.hibernate.Filter filter = entityManager.unwrap(Session.class).enableFilter("limitFilter");
        filter.setParameter("limit", 3);
    }

    public void save(Animal animal) {
        EntityManager entityManager = Database.getEntityManager();
        // TODO: Remove this when we're going to see spring.
        // Spring triggers the validations automatically
        Validation.buildDefaultValidatorFactory().getValidator().validate(animal); // validate bear

        EntityTransaction transaction = entityManager.getTransaction();
        try {
            //begin transaction in case something would break
            transaction.begin();
            entityManager.persist(animal);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        }
    }

    @SuppressWarnings("JpaQueryApiInspection")
    public List<Animal> findAll() {
        EntityManager entityManager = Database.getEntityManager();
        return entityManager.createNamedQuery("findAllAnimals", Animal.class).getResultList();
    }


    public void delete(Animal animal) {
        EntityManager entityManager = Database.getEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            //begin transaction in case something would break
            transaction.begin();
            entityManager.remove(animal);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        }
    }
}
