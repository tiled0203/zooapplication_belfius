package be.belfius.zoo.repository.jpa;

import be.belfius.zoo.domain.Visitor;
import be.belfius.zoo.util.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.Validation;
import java.util.List;

public class VisitorRepository {
    private Logger logger = LoggerFactory.getLogger(VisitorRepository.class.getName());

    public void save(Visitor visitor) { // CREATE
        EntityManager entityManager = Database.getEntityManager();
        Validation.buildDefaultValidatorFactory().getValidator().validate(visitor); // validate bear
        EntityTransaction transaction = entityManager.getTransaction();
        //Use an if statement to check if there are validation errors, if not then save the Bear
        try {
            //begin transaction in case something would break
            transaction.begin();
            entityManager.merge(visitor);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            logger.error(e.getMessage());
        }
    }

    //select v from Visitor v where v.name like :name
    public List<Visitor> findByName(String name) { //READ
        EntityManager entityManager = Database.getEntityManager();
//        return entityManager.createNamedQuery("findVisitorByName", Visitor.class)
//                .setParameter("name", "%" + name + "%")
//                .getResultList();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Visitor> query = criteriaBuilder.createQuery(Visitor.class);
        CriteriaQuery<Visitor> visitorCriteriaQuery = query.select(query.from(Visitor.class))
                .where(criteriaBuilder
                        .like(query.from(Visitor.class).get("name"), "%" + name + "%")
                );

        return entityManager.createQuery(visitorCriteriaQuery).getResultList();
    }
}
