package be.belfius.zoo.repository.generic_jpa_repositories;

import be.belfius.zoo.domain.Visitor;

public class VisitorRepository extends GenericRepository<Visitor, Integer> {


    public VisitorRepository() {
        super(Visitor.class);
    }
}
