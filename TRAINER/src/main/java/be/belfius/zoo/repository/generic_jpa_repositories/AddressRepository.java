package be.belfius.zoo.repository.generic_jpa_repositories;

import be.belfius.zoo.domain.Address;

public class AddressRepository extends GenericRepository<Address, Long> {
    public AddressRepository() {
        super(Address.class);
    }
}
