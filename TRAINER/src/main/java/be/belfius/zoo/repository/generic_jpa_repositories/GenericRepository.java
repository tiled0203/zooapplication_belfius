package be.belfius.zoo.repository.generic_jpa_repositories;

import be.belfius.zoo.domain.BaseEntity;
import be.belfius.zoo.util.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.Validation;
import java.io.Serializable;
import java.util.List;

public abstract class GenericRepository<T extends BaseEntity, ID extends Serializable> {
    private Logger logger = LoggerFactory.getLogger(GenericRepository.class.getName());

    private Class<T> entityClass;

    public GenericRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public T findById(ID id) { // public Animal findById(Interger id)
        EntityManager entityManager = getEntityManager();
        return entityManager.find(entityClass, id);
    }

    public List<T> findByName(String name) {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<T> emptyQuery = criteriaBuilder.createQuery(entityClass);
            Root<T> from = emptyQuery.from(entityClass);
            emptyQuery.select(from).where(criteriaBuilder.like(from.get("name"), "%" + name + "%"));
            return entityManager.createQuery(emptyQuery).getResultList();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public List<T> findAll() {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaQuery<T> emptyQuery = entityManager.getCriteriaBuilder().createQuery(entityClass);
            emptyQuery.select(emptyQuery.from(entityClass));
            return entityManager.createQuery(emptyQuery).getResultList();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public void save(T entity) {
        EntityManager entityManager = getEntityManager();

        // TODO: Remove this when we're going to see spring.
        // Spring triggers the validations automatically
        Validation.buildDefaultValidatorFactory().getValidator().validate(entity); // validate bear

        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.merge(entity); // update or create new entity. it's safer to use this because it could throw a detach exception
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            logger.error(e.getMessage());
        }
    }

    public void delete(T entity) {
        EntityManager entityManager = getEntityManager();

        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            //Entity could be in detached state so we fetch the entity reference back from the db
            entityManager.remove(entityManager.getReference(entityClass, entity.getId()));
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            logger.error(e.getMessage());
        }
    }

    public void deleteById(ID id) {
        EntityManager entityManager = getEntityManager();

        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            //Entity could be in detached state so we fetch the entity reference back from the db
            entityManager.remove(entityManager.getReference(entityClass, id));
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            logger.error(e.getMessage());
        }
    }

    public EntityManager getEntityManager() {
        // i've refactored the getEntityManager in the Database Class
        // because i found out that the entitymanager can still hold old data
        // now with this we create a new EntityManager every time we call this method and also close it first when it's open
        // this is already being solved with the spring framework
        return Database.getEntityManager();
    }

//    {
//        org.hibernate.Filter filter = entityManager.unwrap(Session.class).enableFilter("limitFilter");
//        filter.setParameter("limit", 3);
//    }
}
