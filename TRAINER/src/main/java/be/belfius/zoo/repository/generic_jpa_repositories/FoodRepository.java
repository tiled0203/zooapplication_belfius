package be.belfius.zoo.repository.generic_jpa_repositories;

import be.belfius.zoo.domain.Food;
import be.belfius.zoo.domain.enums.FoodType;

import javax.persistence.EntityManager;
import java.util.List;

public class FoodRepository extends GenericRepository<Food, Integer> {

    public FoodRepository() {
        super(Food.class);
    }

    public List<Food> getFoodByType(FoodType foodType) {
        EntityManager entityManager = getEntityManager();
        try {
            return entityManager.createQuery("select f from Food f where f.foodType = :foodType", Food.class)
                    .setParameter("foodType", foodType).getResultList();
        } finally {
            entityManager.close();
        }
    }

}
