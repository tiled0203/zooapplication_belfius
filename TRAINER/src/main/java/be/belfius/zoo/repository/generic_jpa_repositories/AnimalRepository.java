package be.belfius.zoo.repository.generic_jpa_repositories;

import be.belfius.zoo.domain.Animal;

public class AnimalRepository extends GenericRepository<Animal, Integer> {

    public AnimalRepository() {
        super(Animal.class);
    }
}
