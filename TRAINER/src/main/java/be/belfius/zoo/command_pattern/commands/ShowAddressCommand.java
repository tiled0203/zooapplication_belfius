package be.belfius.zoo.command_pattern.commands;

import be.belfius.zoo.services.AddressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class ShowAddressCommand implements Command {
    private AddressService addressService = new AddressService();
    private Scanner scanner = new Scanner(System.in);
    private static Logger logger = LoggerFactory.getLogger(ShowAddressCommand.class.getName());

    @Override
    public void execute() {
        logger.info("All addresses");
        addressService.findAll().forEach(address -> System.out.printf("Street: %s %s City: %s \n", address.getStreet(), address.getHouseNumber(), address.getCity()));
    }
}
