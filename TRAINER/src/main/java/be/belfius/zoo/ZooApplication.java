package be.belfius.zoo;

import be.belfius.zoo.command_pattern.ZooWorker;
import be.belfius.zoo.services.AddressService;
import be.belfius.zoo.util.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;

import static java.lang.Thread.sleep;

public class ZooApplication {
    private static Logger logger = LoggerFactory.getLogger(ZooApplication.class.getName());
    private AddressService addressService = new AddressService();
    private Scanner scanner = new Scanner(System.in);
    private ZooWorker zooWorker = new ZooWorker();
    private boolean running = true;


    public static void main(String[] args) {
        logger.info("Opening Zoo...");
        new ZooApplication().start();
    }

    private void start() {
        while (running) {
            try {
                Commander.Commands command = chooseCommand();
                executeCommand(command);
            } catch (Exception e) {
                logger.error("Mayday mayday Error! :", e);
            }
        }
        logger.info("Closing zoo...");

        //TODO: Move the following lines to a appropriate methods !
//        bearService.findAll().stream().forEach(bearRow -> System.out.printf("id: %d name: %s \n", bearRow.getId(), bearRow.getName()));
//        Bear foundBearById = bearService.findById(2);
//        System.out.printf("id: %d name: %s \n", foundBearById.getId(), foundBearById.getName());
//
//        bearService.removeById(2);
//
//        bearService.removeByName("Amir");
//
//        Bear bear = new Bear();
//        bear.setName("Tim"); // remove value if you want to test the validation on 'name' property
//        bear.setDateOfBirth(LocalDate.of(1963, 4, 5));
//        bearService.save(bear);

//
//        Visitor visitor = null;
//        Ticket ticket = new Ticket(40.0);
//        for (int i = 0; i < 100; i++) {
//            visitor = new Visitor("Tom" + i, VisitorType.ADULT);
//            Address address = new Address("testStreet", String.valueOf(i), "Kalmthout");
//            Address address2 = new Address("testStreet", String.valueOf(i), "Kalmthout");
//            visitor.setTicket(ticket);
//            visitor.setAddress(Arrays.asList(address, address2));
//            ticket.setReductionPrice(ticketService.calculateReductionPriceForVisitorType(visitor));
//            visitorService.save(visitor);
//        }
//
//        Ticket ticket1 = ticketService.findTicketById(1);
//        ticket1.getVisitors().forEach(visitor1 -> System.out.println(visitor1.getName() + " adr:" + visitor1.getAddress()));
    }

    private void executeCommand(Commander.Commands command) {

        //Example 1: Command pattern with reflection api
//        try {
//            Class c = Class.forName(Command.class.getName());
//            Method[] m = c.getDeclaredMethods();
//            Method commandMethod = Arrays.stream(m)
//                    .filter(method -> method.getName().equals(command.getCommandMethod()))
//                    .findFirst()
//                    .orElseThrow(() -> new NoSuchMethodError("method doesn't exist"));
//            commandMethod.invoke(new Command());
//        } catch (ClassNotFoundException | IllegalAccessException | InvocationTargetException e) {
//            logger.error("Mayday crashing down!!", e);
//        }
        //Example 2: Normal command pattern using ZooWorker = invoker, ShowAnimalCommand and others = command , AnimalService and others = Receiver
        zooWorker.execute(command.getCommand());

    }

    Commander.Commands chooseCommand() throws InterruptedException {
        System.out.println("Available commands:");
        for (Commander.Commands command : Commander.Commands.values()) {
            System.out.printf("\t%d. %s \n", command.ordinal(), command.getCommandDescription()); //instead of using a hard coded index, i'm using ordinal to get the position of the enum.
        }
        System.out.print("Choose a command: ");
        int command = scanner.nextInt();
        Optional<Commander.Commands> commandOptional = Arrays.stream(Commander.Commands.values()).filter(command1 -> command1.ordinal() == command).findFirst(); //ordinal can be used to fetch the correct command
        if (!commandOptional.isPresent()) {
            logger.warn("Give in a correct number");
            sleep(2000);
            chooseCommand();
        }
        return commandOptional.get();
    }

    {
        //we need to create an entityManager
        //just to let hibernate trigger the generation of the tables and pre fill it, this is not needed for the spring framework
        Database.getEntityManager();
    }

}
